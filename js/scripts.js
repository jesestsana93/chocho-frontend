$(function () {
    /*-----------------------------------------------------------
    1. FUNCIONES PARA EL BOTON MOBIL DEL MENU 
    -------------------------------------------------------------*/
    $('#menu-navegacion .navbar-toggler').click(function () {
        $('.boton-menu').toggleClass('icono-cerrar');
    });

    $('#menu-navegacion .navbar-nav a').click(function () {
        $('.boton-menu').removeClass('icono-cerrar');
        $('#menu-navegacion .navbar-collapse').collapse('hide');
    });

    /*-----------------------------------------------------------
    2. INICIANDO "page-scroll-to-id" para navegacion
    -------------------------------------------------------------*/
   $('#menu-principal a').mPageScroll2id({
        offset: 80,
        highlightClass: 'active'
    });

    /*---------------------------------
    3. CABECERA ANIMADA
    ----------------------------------*/
    $(window).scroll(function () {
        var nav = $('.transparente');
        var scroll = $(window).scrollTop();
        if (scroll >= 200) {
            nav.addClass("barra-navegacion");
        } else {
            nav.removeClass("barra-navegacion");
        }
    });

});