<?php

if(isset($_POST['submit_btn']))://valida que elsubmit venga del boton enviar
	//Recibo los datos y los almaceno en variables
	//var_dump($_POST);
	//Datos de como se enteraron
	$medios = $_POST['medios'];
	$respuesta = implode(', ', $medios); //implode une elementos de un array en un string
	
	//Datos de informacion personal 
	//echo $Nombre = $_POST["Nombre"];
	$Nombre = $_POST["Nombre"];
	$Apellidos = $_POST["Apellidos"];
	$Fecha = date('Y-m-d H:i:s');
	$Edad = $_POST["Edad"];
	$Sexo = $_POST["Sexo"];
	$Estudios_nivel = $_POST["Estudios_nivel"];
	$Diagnostico_actual = $_POST["Diagnostico_actual"];
	$Nombre_medico = $_POST["Nombre_medico"];
	$Institutcion_medica = $_POST["Institutcion_medica"];
	$Apoyo_gubernamental = $_POST["Apoyo_gubernamental"];
	$Apoyo_brindado = $_POST["Apoyo_brindado"];
	$Tipo_apoyo_chocho = $_POST["Tipo_apoyo_chocho"];
	//$foto = addslashes(file_get_contents($_FILES['foto']['tmp_name'])
	$foto = $_FILES['foto']['name'];
	$tmp_dir = $_FILES['foto']['tmp_name'];
  	$imgSize = $_FILES['foto']['size'];

  	$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
  
   // valid image extensions
   $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
   
	//$foto = $_POST["foto"];

	//Datos de localizacion
	$Nombre_tutor = $_POST["Nombre_tutor"];
	$Telefono = $_POST["Telefono"];
	$Celular = $_POST["Celular"];
	$Correo_electronico = $_POST["Correo_electronico"];
	$Calle = $_POST["Calle"];
	$Numero_interior = $_POST["Numero_interior"];
	$Numero_exterior = $_POST["Numero_exterior"];
	$Municipio = $_POST["Municipio"];
	$CP = $_POST["CP"];
	$Entidad_federatival = $_POST["Entidad_federatival"];
	$foto_beneficiario = $_POST["foto_beneficiario"];

	//Datos de situacion socio-economica
	$Casa_propia = $_POST["Casa_propia"];
	$Tipo_piso = $_POST["Tipo_piso"];
	$Tipo_techo = $_POST["Tipo_techo"];
	$Internet = $_POST["Internet"];
	$Numero_personas = $_POST["Numero_personas"];
	$Personas_mayores = $_POST["Personas_mayores"];
	$Ingreso_mensual = $_POST["Ingreso_mensual"];
	$Ingreso_enfermedades = $_POST["Ingreso_enfermedades"];
	$tiempo_traslado = $_POST["tiempo_traslado"];

	try{ 
		require_once('bd_conexion.php');	

		//$query = "INSERT INTO datos (medios) VALUES ('$respuesta')";
		//$query_run = mysqli_query($conn, $query);

		/*****************************************************************************
		    * INSERCION para la tabla datos:
		    * Un marcador ? que sustituye al valor real neutralizando así la inyección
		******************************************************************************/
		$datos = $conn->prepare("INSERT INTO datos (medios) VALUES (?)");
		$datos->bind_param("s", $respuesta);
		$datos->execute();
		//Mensaje de exito o error en la inserción de los datos a la tabla datos
		if($datos) {
	    	echo "* Datos insertados correctamente en la base de datos de los medios donde se entero <br>";
		}else{
	    	echo "* ¡Error al insertar los datos en la base de datos del medio donde se entero! <br>";
		}

		/*****************************************************************************
		    * INSERCION para la tabla info_personal
		*****************************************************************************/
		$personal = $conn->prepare("INSERT INTO info_personal (nombre, apellidos, fecha, edad, sexo, estudios_nivel, diagnostico_actual, nombre_medico, institutcion_medica, apoyo_gubernamental, apoyo_brindado,tipo_apoyo_chocho,foto) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
		
		$personal->bind_param("sssisssssssss", $Nombre, $Apellidos, $Fecha, $Edad, $Sexo, $Estudios_nivel, $Diagnostico_actual, $Nombre_medico, $Institutcion_medica, $Apoyo_gubernamental, $Apoyo_brindado, $Tipo_apoyo_chocho, $foto);

		$personal->execute();
		//Mensaje de exito o error en la inserción de los datos a la tabla info_personal
		if ($personal) {
			echo '* Datos de informacion personal insertados correctamente en la base de datos <br><br>';
		} else {
			echo '* ¡Error al registrar los datos de informacion personal en la base de datos! <br><br>';			
		}

		/*************************************************************************
		    * INSERCION para la tabla localizacion
		**************************************************************************/
		$localizacion = $conn->prepare("INSERT INTO localizacion (nombre_tutor, telefono, celular, correo_electronico, calle, numero_interior, numero_exterior, municipio, c.p., entidad_federatival, foto_beneficiario) VALUES (?,?,?,?,?,?,?,?,?,?,?)");

		$localizacion->bind_param("siissiisiss", $Nombre_tutor, $Telefono, $Celular, $Correo_electronico, $Calle, $Numero_interior, $Numero_exterior, $Municipio, $CP, $Entidad_federatival, $foto_beneficiario);		
		
		$localizacion->execute();
		//Mensaje de exito o error en la inserción de los datos a la tabla localizacion
		if ($localizacion) {
			echo '* Datos de localizacion registrados exitosamente <br><br>';
		} else {
			echo '* Error al registrar los datos de localizacion <br><br>';			
		}

		/*$localizacion = "INSERT INTO localizacion (nombre_tutor, telefono, celular, correo_electronico, calle, numero_interior, numero_exterior, municipio, cp, entidad_federatival,  foto_beneficiario) VALUES ('$Nombre_tutor','$Telefono','$Celular','$Correo_electronico','$Calle','$Numero_interior','$Numero_exterior','$Municipio', $CP', '$Entidad_federatival', '$foto_beneficiario')";

		echo $localizacion."<br><br>";

		$resultado2 = mysqli_query($conn, $localizacion);
		var_dump($resultado2);

		if ($resultado2) {
			echo '* Datos de localizacion registrados exitosamente en la base de datos <br><br>';
		} else {
			echo '* ¡Error al registrar los datos de localizacion en la base de datos! <br><br>';			
		}*/

		/*************************************************************************
		    * INSERCION para la tabla situacion_socioeconomica
		**************************************************************************/
		$situacion = $conn->prepare("INSERT INTO situacion_socioeconomica (casa_propia, tipo_piso, tipo_techo, internet, numero_personas, personas_mayores, ingreso_mensual, ingreso_enfermedades, tiempo_traslado) VALUES (?,?,?,?,?,?,?,?,?)");
		
		$situacion->bind_param("ssssiidds", $Casa_propia, $Tipo_piso, $Tipo_techo, $Internet, $Numero_personas, $Personas_mayores, $Ingreso_mensual, $Ingreso_enfermedades, $tiempo_traslado);

		$situacion->execute();		
		//Mensaje de exito o error en la inserción de los datos a la tabla situacion
		if ($situacion) {
			echo '* Datos de situacion socioeconomica registrados exitosamente en la base de datos <br><br>';
		} else {
			echo '* ¡Error al registrar los datos de situacion socioeconomica en la base de datos! <br><br>';
		}

		//cerrando las conexiones de las inserciones a las tablas
		$datos->close();
		$personal->close();
		//$localizacion->close();
		$situacion->close();

		//cerrando la conexion a la base de datos
	 	$conn->close();

	}catch(Excepcion $e){
		$error = $e->getMessage();
	}

endif;

?>